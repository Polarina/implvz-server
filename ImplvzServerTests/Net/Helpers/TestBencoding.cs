﻿using Implvz.Net.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;
using System.Text;

namespace ImplvzServerTests.Net.Helpers
{
    [TestClass]
    public class TestBencoding
    {
        #region Helpers

        private static bool AreEqual(BElement a, BElement b)
        {
            if (a.GetType() != b.GetType())
                return false;

            if (a is BDictionary)
            {
                var a1 = a as BDictionary;
                var b1 = b as BDictionary;

                if (a1.Count != b1.Count)
                    return false;

                foreach (var a2 in a1)
                {
                    BElement value;

                    if (b1.TryGetValue(a2.Key, out value) == false)
                        return false;

                    if (AreEqual(a2.Value, value) == false)
                        return false;
                }
            }

            if (a is BInteger)
            {
                var a1 = a as BInteger;
                var b1 = b as BInteger;

                return a1.Value == b1.Value;
            }

            if (a is BList)
            {
                var a1 = a as BList;
                var b1 = b as BList;

                if (a1.Count != b1.Count)
                    return false;

                for (var i = 0; i < a1.Count; ++i)
                {
                    if (AreEqual(a1[i], b1[i]) == false)
                        return false;
                }
            }

            if (a is BString)
            {
                var a1 = a as BString;
                var b1 = b as BString;

                if (a1.Value.Length != b1.Value.Length)
                    return false;

                return a1.Value.SequenceEqual(b1.Value);
            }

            return true;
        }

        /// <summary>
        /// This is a helper for almost all bencoding related tests.
        /// </summary>
        /// <param name="teststring">bencoded string to test</param>
        /// <param name="test">set to null for testing failure</param>
        private static void TestHelper(string teststring, BElement test)
        {
            var check = Encoding.UTF8.GetBytes(teststring);

            try
            {
                for (var i = 0; i < teststring.Length; i++)
                {
                    var testThis = teststring.Substring(0, i);

                    Assert.IsNull(BencodeDecoder.Decode(Encoding.UTF8.GetBytes(testThis)));
                }

                var result = BencodeDecoder.Decode(check);

                if (test == null)
                    Assert.Fail();

                Assert.IsNotNull(result);
                Assert.AreEqual(result.Item2, teststring.Length);

                if (AreEqual(test, result.Item1) == false)
                    Assert.Fail();
            }
            catch (BencodingException)
            {
                if (test != null)
                    Assert.Fail();
            }

            if (test != null)
            {
                var stream = new MemoryStream(check.Length);
                test.ToBencodedString(stream);

                Assert.IsTrue(check.SequenceEqual(stream.ToArray()));
            }
        }

        #endregion

        #region BDictionary

        [TestMethod]
        public void Dictionary()
        {
            TestHelper("de", new BDictionary());
        }

        [TestMethod]
        public void DictionaryDuplicateKeys()
        {
            TestHelper("d3:key5:value3:key4:dirte", null);
        }

        [TestMethod]
        public void DictionaryGetSet()
        {
            var test = new BDictionary {{"Gunnar", new BString("Guðvarðarson")}};
            Assert.IsTrue(test["Gunnar"].ToString() == "Guðvarðarson");

            test["Gunnar"] = new BString("rokkar");
            Assert.IsTrue(test["Gunnar"].ToString() == "rokkar");
            test["Gabríel"] = new BInteger(19);

            try
            {
                var x = test["rannuG"];
                Assert.Fail();
            }
            catch (BencodeKeyNotFoundException)
            {
            }
        }

        [TestMethod]
        public void DictionaryUnsorted()
        {
            TestHelper("d2:bai3e4:aqrelee", null);
        }

        [TestMethod]
        public void DictionaryWithEmptyKey()
        {
            TestHelper("d0:0:e", new BDictionary
                {
                    {"", new BString("")}
                });
        }

        [TestMethod]
        public void DictionaryWithValue()
        {
            TestHelper("d7:someKey9:someValuee", new BDictionary
                {
                    {"someKey", new BString("someValue")}
                });
        }

        #endregion

        #region BInteger

        [TestMethod]
        public void Integer()
        {
            TestHelper("i42e", new BInteger(42));
        }

        [TestMethod]
        public void IntegerCompareTo()
        {
            var test1 = new BInteger(42);
            var test2 = new BInteger(43);

            Assert.IsTrue(test1.CompareTo(test1) == 0);
            Assert.IsTrue(test1.CompareTo(test2) < 0);
            Assert.IsTrue(test2.CompareTo(test1) > 0);
        }

        [TestMethod]
        public void IntegerEquals()
        {
            var test = new BInteger(42);
            Assert.IsTrue(test.Equals(test));

            var other = new BInteger(43);
            Assert.IsFalse(test.Equals(other));

            var dict = new BDictionary();
            Assert.IsFalse(test.Equals(dict));
        }

        [TestMethod]
        public void IntegerHashCode()
        {
            Assert.IsTrue(new BInteger(42).GetHashCode() == 42);
        }

        [TestMethod]
        public void IntegerInvalid()
        {
            TestHelper("ice", null);
        }

        [TestMethod]
        public void IntegerNegative()
        {
            TestHelper("i-42e", new BInteger(-42));
        }

        [TestMethod]
        public void IntegerNegativeZero()
        {
            TestHelper("i-0e", null);
        }

        [TestMethod]
        public void IntegerOperator()
        {
            const int test = 42;
            BInteger bla = test;

            Assert.IsTrue(bla.ToFloat() == 42/1000000.0f);
        }

        [TestMethod]
        public void IntegerTooLarge()
        {
            TestHelper("i9223372036854775808e", null);
        }

        [TestMethod]
        public void IntegerToString()
        {
            Assert.IsTrue(new BInteger(42).ToString() == "42");
        }

        [TestMethod]
        public void IntegerZeroPadded()
        {
            TestHelper("i03e", null);
        }

        #endregion

        #region BList

        [TestMethod]
        public void List()
        {
            TestHelper("le", new BList());
        }

        [TestMethod]
        public void ListAdd()
        {
            var list1 = new BList();
            list1.Add(new[] {"Gunnar"});

            var list2 = new BList {new BString("Gunnar")};

            Assert.IsTrue(AreEqual(list1, list2));

            var list3 = new BList();
            list3.Add(new long[] {42});
            var list4 = new BList() {new BInteger(42)};

            Assert.IsTrue(AreEqual(list3, list4));

            var list5 = new BList { new BInteger[] { 1, 2, 3 }};
            var list6 = new BList { new long[] { 1, 2, 3 } };

            Assert.IsTrue(AreEqual(list5, list6));
        }

        [TestMethod]
        public void ListWithValue()
        {
            TestHelper("l4:lolsi42ee", new BList
                {
                    new BString("lols"),
                    new BInteger(42)
                });
        }

        #endregion

        #region BString

        [TestMethod]
        public void String()
        {
            TestHelper("5:Gunni", new BString("Gunni"));
        }

        [TestMethod]
        public void StringCompareTo()
        {
            var test1 = new BString("Gunni");
            var test2 = new BString("Gunnar");
            var test3 = new BString("GunnarG");

            Assert.IsTrue(test1.CompareTo(test1) == 0);
            Assert.IsTrue(test1.CompareTo(test2) > 0);
            Assert.IsTrue(test2.CompareTo(test3) < 0);
            Assert.IsTrue(test3.CompareTo(test2) > 0);
        }

        [TestMethod]
        public void StringEquals()
        {
            var test1 = new BString("Gunnar");
            var test2 = new BString("Gabríel");
            var test3 = new BInteger(42);

            Assert.IsTrue(test1.Equals(test1));
            Assert.IsFalse(test1.Equals(test2));
            Assert.IsFalse(test1.Equals(test3));
        }

        [TestMethod]
        public void StringHashCode()
        {
            var bla = new BString("Gunnar");

            Assert.IsTrue(bla.GetHashCode() == bla.Value.GetHashCode());
        }

        [TestMethod]
        public void StringLengthInvalid()
        {
            TestHelper("6c:Gunnar", null);
        }

        [TestMethod]
        public void StringLengthTooLarge()
        {
            TestHelper("2147483648:SOMETHING", null);
        }

        [TestMethod]
        public void StringOperator()
        {
            const string test1 = "Gunnar";
            BString bla1 = test1;

            Assert.IsTrue(bla1.ToString() == test1);
            Assert.IsTrue(Encoding.UTF8.GetString(bla1.ToBencodedString(new MemoryStream()).ToArray()) == "6:Gunnar");

            byte[] test2 = Encoding.UTF8.GetBytes("Gunnar");
            BString bla2 = test2;

            Assert.IsTrue(bla2.ToString() == Encoding.UTF8.GetString(test2));
        }

        [TestMethod]
        public void StringWithZeroPaddedLength()
        {
            TestHelper("06:String", null);
        }

        #endregion

        #region Other

        [TestMethod]
        public void InvalidBencode()
        {
            TestHelper("baunir", null);
        }

        [TestMethod]
        public void Null()
        {
            Assert.IsNull(BencodeDecoder.Decode(null));
        }

        #endregion
    }
}
