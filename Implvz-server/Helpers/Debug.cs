﻿using System;
using System.Text;

namespace Implvz.Helpers
{
	static class Debug
	{
		public enum DebugLevels
		{
			Error    = 1,  // 00001
			Warning  = 2,  // 00010
			Info     = 4,  // 00100
			Debug    = 8,  // 01000
            Protocol = 16, // 10000
            Always   = 63  // 11111
		}

		public static void WriteLine(object stuff, DebugLevels debugLevel = DebugLevels.Debug, ConsoleColor color = ConsoleColor.Yellow, string prefix = "--")
		{
			var actual = Properties.Settings.Default.debugLevel;
			var level  = (uint) debugLevel;

			if ((level & actual) <= 0) return;

			if (stuff.GetType() == typeof(byte[]))
			{
				var str = Encoding.UTF8.GetString((byte[]) stuff);

				for (var i = 0; i <= 0x1F; i++)
				{
					str = str.Replace((Char)i, ' ');
				}

				str = str.Replace((Char)0x7F, ' ');

				if (str.Length > 140)
					str = str.Substring(0, 140);

				DebugConsole.WriteLine(color, prefix, "{0}", str);
			}
			else
			{
				DebugConsole.WriteLine(color, prefix, stuff);
			}
		}
	}
}
