﻿using System;
using System.Threading;

namespace Implvz.Helpers
{
	public static class DebugConsole
	{
		private static readonly object Writing = new Object();

		public static void Write(ConsoleColor color, object src, string format, params object[] args)
		{
			PerformWrite(color, src, String.Format(format, args));
		}

		public static void WriteLine(ConsoleColor color, object src, string format, params object[] args)
		{
			PerformWrite(color, src, String.Format(format + "\n", args));
		}

		public static void Write(ConsoleColor color, object src, object data)
		{
			PerformWrite(color, src, data.ToString());
		}

		public static void WriteLine(ConsoleColor color, object src, object data)
		{
			PerformWrite(color, src, data.ToString() + "\n");
		}

		private static void PerformWrite(ConsoleColor color, object src, string data)
		{
			lock (Writing)
			{
				ConsoleColor bak = Console.ForegroundColor;

				Console.ForegroundColor = color;
				Console.Write(src);

				Console.ForegroundColor = ConsoleColor.White;
				Console.Write("[{0,3}]", Thread.CurrentThread.ManagedThreadId);

				Console.ForegroundColor = ConsoleColor.Red;
				Console.Write("::");

				Console.ForegroundColor = color;
				Console.Write(data);

				Console.ForegroundColor = bak;
			}
		}
	}
}
