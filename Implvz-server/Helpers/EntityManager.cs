﻿using System;
using System.Linq;
using Implvz.Game.Objects;
using System.Collections.Generic;

namespace Implvz.Helpers
{
    public sealed class EntityManager
    {
        private readonly      List<Entity> _entities;
        private readonly      Map          _map;

		public  delegate void EntityMove(Entity e);
        public  event         EntityMove DoMoveEntity;

		public  delegate void EntityRemoved(Entity e);
        public  event         EntityRemoved DoRemoveEntity;

        public EntityManager(Map map)
        {
            _entities = new List<Entity>();
            _map      = map;
        }

        public void CreateEntity(Entity e, Entity.HealthChanged f)
        {
            _entities.Add(e);
            e.DoChangeHealth += f;
        }

        public void RemoveEntity(long id)
        {
            var entity = _entities.Find(e => e.ID == id);

            if (entity == null)
                return;

            DoRemoveEntity(entity);
            _entities.Remove(entity);
        }

        public void UpdateEntities()
        {
            var entities = _entities.Where(e => ! e.Tick(_map).Item2).ToArray();

            foreach (var entity in entities)
            {
                RemoveEntity(entity.ID);
            }

            var ents = _entities.ToArray();

            for (var i = 0; i < ents.Count(); i++)
            {
                for (var j = i + 1; j < ents.Count(); j++)
                {
                    var ent1 = ents[i];
                    var ent2 = ents[j];

                    var distance = Math.Sqrt(
                        Math.Pow((ent2.Position.X - ent1.Position.X), 2) +
                        Math.Pow((ent2.Position.Y - ent1.Position.Y), 2));

                    if ( ! (distance - ent1.Radius - ent2.Radius <= 0))
                        continue;

                    if ( ! (ent1 is Player) && ent1.Collision(ent2))
                        RemoveEntity(ent1.ID);

                    if ( ! (ent2 is Player) && ent2.Collision(ent1))
                        RemoveEntity(ent2.ID);
                        
                    if (ent1 is Player && ent1.Collision(ent2))
                    {
                        ent1.NewPosition(_map.GetSpawnLocation(), 0);
                        (ent1 as Player).Respawn(_map.GetSpawnLocation(), 0);
                        DoMoveEntity(ent1);
                        DoMoveEntity(ent1);
                    }

                    if (ent2 is Player && ent2.Collision(ent1))
                    {
                        ent2.NewPosition(_map.GetSpawnLocation(), 0);
                        (ent2 as Player).Respawn(_map.GetSpawnLocation(), 0);
                        DoMoveEntity(ent2);
                        DoMoveEntity(ent2);
                    }
                }
            }

            // TODO health calculations
        }
    }
}
