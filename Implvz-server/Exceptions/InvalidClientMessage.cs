﻿using System;

namespace Implvz.Exceptions
{
    [Serializable]
	public class InvalidClientMessage : Exception
	{
		public InvalidClientMessage(string message) : base(message) { }

		public InvalidClientMessage(string message, Exception inner) : base(message, inner) { }
	}
}
