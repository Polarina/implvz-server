﻿using System;

namespace Implvz.Exceptions
{
    [Serializable]
	public class NoSpawnLocationException : FormatException { }
}
