﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Implvz.Net
{
    public sealed class TCPServer : IDisposable
    {
        private const int InitialCapacity = 16;
        private const int BufferSize = 4096;

        private readonly EndPoint                                                _bind;
        private Socket                                                           _listen;

        private readonly List<Socket>                                            _reads;
        private readonly List<Socket>                                            _writes;

        private readonly Dictionary<Socket, byte[]>                              _buffers;
        private readonly Dictionary<Socket, LinkedList<Tuple<byte[], int, int>>> _queue;

        private readonly Queue<byte[]>                                           _bufferPool;

		public delegate void ConnectHandler(TCPServer sender, Socket client);
		public delegate void DisconnectHandler(TCPServer sender, Socket client);
		public delegate void RecieveHandler(TCPServer sender, Socket client, byte[] data, int offset, int bytes);
        
		public event ConnectHandler    OnConnect;
        public event DisconnectHandler OnDisconnect;
        public event RecieveHandler    OnRecieve;

        public TCPServer(EndPoint bind)
        {
            _bind       = bind;
            _reads      = new List<Socket>(InitialCapacity);
            _writes     = new List<Socket>(InitialCapacity);

            _buffers    = new Dictionary<Socket, byte[]>(InitialCapacity);
            _queue      = new Dictionary<Socket, LinkedList<Tuple<byte[], int, int>>>(InitialCapacity);

            _bufferPool = new Queue<byte[]>(InitialCapacity);

            for (var i = 0; i < InitialCapacity; i++)
                _bufferPool.Enqueue(new byte[BufferSize]);
        }

        private byte[] GetBuffer()
        {
            if (_bufferPool.Count == 0)
            {
                for (var i = 0; i < InitialCapacity; i++)
                    _bufferPool.Enqueue(new byte[BufferSize]);
            }

            return _bufferPool.Dequeue();
        }

        private void ReturnBuffer(byte[] buffer)
        {
            _bufferPool.Enqueue(buffer);
        }

        public void Start()
        {
            _listen = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _listen.Bind(_bind);
            _listen.Listen(100);

            _reads.Add(_listen);
        }

        public void Iterate()
        {
            var rs = new List<Socket>(_reads);
            Socket.Select(rs, null, null, 0);

            foreach (var r in rs)
            {
                if (r == _listen)
                {
                    var client = r.Accept();
                    client.NoDelay = true;

                    _reads.Add(client);
                    _buffers.Add(client, GetBuffer());
                    _queue.Add(client, new LinkedList<Tuple<byte[], int, int>>());

                    OnConnect(this, client);
                }
                else
                {
                    var buffer = _buffers[r];

                    try
                    {
                        var bytes = r.Receive(buffer);

                        if (bytes > 0)
                            OnRecieve(this, r, buffer, 0, bytes);
                        else
                            Disconnect(r);
                    }
                    catch (SocketException)
                    {
                        Disconnect(r);
                    }
                }
            }

            var ws = new List<Socket>(_writes);

            if (ws.Count == 0)
                return;

            Socket.Select(null, ws, null, 0);

            foreach (var w in ws)
            {
                var queue = _queue[w];
                var message = queue.First.Value;

                try
                {
                    var bytes = w.Send(message.Item1, message.Item2, message.Item3, SocketFlags.None);

                    if (bytes < message.Item3)
                        queue.First.Value = new Tuple<byte[], int, int>(message.Item1, message.Item2 + bytes, message.Item3 - bytes);
                    else
                        queue.RemoveFirst();
                }
                catch (SocketException)
                {
                    Disconnect(w);
                }

                if (queue.Count == 0)
                    _writes.Remove(w);
            }
        }

        public void Disconnect(Socket client)
        {
            OnDisconnect(this, client);

            client.Close();

            ReturnBuffer(_buffers[client]);

            _reads.Remove(client);
            _writes.Remove(client);
            _buffers.Remove(client);
            _queue.Remove(client);
        }

        public void Send(Socket client, byte[] data, int offset, int bytes)
        {
            if (bytes == 0)
                return;

            var queue = _queue[client];

            if (queue.Count == 0)
                _writes.Add(client);

            queue.AddLast(new Tuple<byte[], int, int>(data, offset, bytes));
        }

        public void Dispose()
        {
            _listen.Close();
        }
    }
}
