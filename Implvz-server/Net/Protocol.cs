﻿using System.IO;
using System.Linq;
using Implvz.Game.Objects;
using Implvz.Net.Helpers;
using System.Collections.Generic;

namespace Implvz.Net
{
	public static class Protocol
	{
	    public enum ClientMessage
		{
			ServerList           = 0x00,
			Login                = 0x01,
			//changeName         = 0x02, // removed
			Move                 = 0x03,
			ChatMessage          = 0x04,
			//disconnect         = 0x05  // removed
			Weapon               = 0x06,
			Fire                 = 0x07
		}

	    private enum ServerMsg
		{
			ServerList           = 0x00,
			//loginFailure       = 0x01, // removed
			//changeName         = 0x02, // removed
			//changeNameFailure  = 0x03, // removed
			Move                 = 0x04,
			//moveFailure        = 0x05, // removed
			ChatMessage          = 0x06,
			Kick                 = 0x07,
			//ClientDisconnected = 0x08, // removed
			ClientConnected      = 0x09,
			Map                  = 0x0A,
			Health               = 0x0B,
            RemoveEntity         = 0x0C,
            Score                = 0x0D
		}

		public static byte[] MkServerList(int current, long max)
		{
			return BuildMessage(ServerMsg.ServerList, new Dictionary<string, BElement>()
			{
				{ "current", (BInteger) current },
				{ "max"    , (BInteger) max     }
			});
		}

		public static byte[] MkMove(Entity player)
		{
			return BuildMessage(ServerMsg.Move, new Dictionary<string, BElement>()
			{
				{ "entity", (BInteger) player.ID        },
				{ "x",      MkFloat(player.Position.X)  },
				{ "y",      MkFloat(player.Position.Y)  },
				{ "rot",    MkFloat(player.Orientation) }
			});
		}

		public static byte[] MkChatMessage(Player player, string message)
		{
			return BuildMessage(ServerMsg.ChatMessage, new Dictionary<string, BElement>()
			{
				{ "player",  (BString) player.Name },
				{ "message", (BString) message     }
			});
		}

		public static byte[] MkChatMessage(Player player, BElement message)
		{
			return BuildMessage(ServerMsg.ChatMessage, new Dictionary<string, BElement>()
			{
				{ "player",  (BString) player.Name },
				{ "message", message               }
			});
		}

		public static byte[] MkKick(string reason)
		{
			return BuildMessage(ServerMsg.Kick, new Dictionary<string, BElement>()
			{
				{ "reason", (BString) reason }
			});
		}

		public static byte[] MkClientConnected(long[] ids, string[] names)
		{
            var playerIDs = new BList { ids };
			var clients   = new BList { names };

			return BuildMessage(ServerMsg.ClientConnected, new Dictionary<string, BElement>()
			{
                { "ids",   playerIDs },
				{ "names", clients   }
			});
		}

		public static byte[] MkMap(byte[] compressedMap)
		{
			return BuildMessage(ServerMsg.Map,  new Dictionary<string, BElement>()
			{
				{ "map", (BString) compressedMap }
			});
		}

        public static byte[] MkHealth(Player[] players)
        {
            var playerNames   = new BList { players.Select(pn => pn.Name).ToArray() };
            var playerHealths = new BList { players.Select(ph => MkFloat(ph.Health)).ToArray() };

            return BuildMessage(ServerMsg.Health, new Dictionary<string, BElement>()
            {
                { "entity", playerNames   },
                { "health", playerHealths }
            });
        }

        public static byte[] MkRemoveEntity(Entity e)
        {
            return BuildMessage(ServerMsg.RemoveEntity, new Dictionary<string, BElement>()
            {
                { "entity", (BInteger) e.ID }
            });
        }

        public static byte[] MkScore(string[] players, long[] scores)
        {
            var playerNames  = new BList { players };
            var playerscores = new BList { scores };

            return BuildMessage(ServerMsg.Score, new Dictionary<string, BElement>()
            {
                { "players", playerNames  },
                { "scores",  playerscores }
            });
        }

	    private static byte[] BuildMessage(ServerMsg messageId, Dictionary<string, BElement> values)
		{
			var bd = new BDictionary {
				{"", new BInteger((long) messageId)}
			};

			foreach (var value in values)
				bd.Add(value.Key, value.Value);

            return bd.ToBencodedString(new MemoryStream()).ToArray();
		}

		private static BInteger MkFloat(float x)
		{
			return (int) (x * 1000000.0f);
		}
	}
}
