﻿using System;

public class InvalidClientMessage : Exception
{
	public InvalidClientMessage() { }

	public InvalidClientMessage(string message) : base(message) { }

	public InvalidClientMessage(string message, Exception inner) : base(message, inner) { }
}
