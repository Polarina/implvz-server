//
// Encoding usage:
// 
// new BDictionary()
// {
//  {"Some Key", "Some Value"},
//  {"Another Key", 42}
// }.ToBencodedString();
// 
// Decoding usage:
// 
// BencodeDecoder.Decode("d8:Some Key10:Some Value13:Another Valuei42ee");
// 
// Feel free to use it.
// More info about Bencoding at http://wiki.theory.org/BitTorrentSpecification#bencoding
// 
// Originally posted at http://snipplr.com/view/37790/ by SuprDewd.
//

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Implvz.Net.Helpers
{
	public static class BencodeDecoder
	{
		public static Tuple<BElement, int> Decode(byte[] bencodedString)
		{
			var index = 0;

			if (bencodedString == null)
				return null;

			var belement = ReadElement(ref bencodedString, ref index);

            if (belement == null)
			    return null;

			return new Tuple<BElement,int>(belement, index);
		}

		private static BElement ReadElement(ref byte[] bencodedString, ref int index)
		{
            if (bencodedString.Length <= index)
                return null;

            switch (bencodedString[index])
			{
				case (byte) '0':
				case (byte) '1':
				case (byte) '2':
				case (byte) '3':
				case (byte) '4':
				case (byte) '5':
				case (byte) '6':
				case (byte) '7':
				case (byte) '8':
				case (byte) '9': return ReadString(ref bencodedString, ref index);
				case (byte) 'i': return ReadInteger(ref bencodedString, ref index);
				case (byte) 'l': return ReadList(ref bencodedString, ref index);
				case (byte) 'd': return ReadDictionary(ref bencodedString, ref index);
				default: throw Error();
			}
		}

		private static BDictionary ReadDictionary(ref byte[] bencodedString, ref int index)
		{
			index++;
            if (index >= bencodedString.Length)
                return null;

            var dict = new BDictionary();

		    var previous = new BString("");

			while (bencodedString[index] != 'e')
			{
				var k = ReadString(ref bencodedString, ref index);
                if (k == null)
                    return null;

                if (previous.CompareTo(k) > 0)
                    throw Error();

			    previous = k;

				var v = ReadElement(ref bencodedString, ref index);
                if (v == null)
                    return null;

			    try
			    {
                    dict.Add(k, v);
			    }
			    catch (ArgumentException)
			    {
			        throw Error();
			    }

                if (index >= bencodedString.Length)
                    return null;
			}

			index++;
			return dict;
		}

		private static BList ReadList(ref byte[] bencodedString, ref int index)
		{
			index++;
			var lst = new BList();

            if (index >= bencodedString.Length)
                return null;

			while (bencodedString[index] != 'e')
			{
				var element = ReadElement(ref bencodedString, ref index);
                    
                if (element == null)
                    return null;

                lst.Add(element);

                if (index >= bencodedString.Length)
                    return null;
            }

			index++;
			return lst;
		}

		private static BInteger ReadInteger(ref byte[] bencodedString, ref int index)
		{
			index++;

			var end = Array.IndexOf(bencodedString, (byte) 'e', index);
			
			if (end == -1)
			{
				try
				{
                    if (bencodedString.Length == index)
                        return null;

                    if (bencodedString[index] == '-' && bencodedString.Length - 1 == index)
                        return null;

					Int64.Parse(Encoding.ASCII.GetString(bencodedString, index, bencodedString.Length - index),
                        NumberStyles.Any, CultureInfo.InvariantCulture);

                    return null;
				}
				catch (FormatException)
				{
					throw Error();
				}
				catch (OverflowException)
				{
					throw Error();
				}
			}

		    var str = Encoding.ASCII.GetString(bencodedString, index, end - index);
            var integer = Int64.Parse(str, NumberStyles.Any, CultureInfo.InvariantCulture);

            if (integer.ToString() != str)
                throw Error();

			index = end + 1;

			return new BInteger(integer);
		}

		private static BString ReadString(ref byte[] bencodedString, ref int index)
		{
		    var colon = Array.IndexOf(bencodedString, (byte) ':', index);
				
			if (colon == -1)
			{
				try
				{
					Int32.Parse(Encoding.ASCII.GetString(bencodedString, index, bencodedString.Length - index));
					    
                    return null;
				}
				catch (FormatException)
				{
					throw Error();
				}
				catch (OverflowException)
				{
					throw Error();
				}
			}

		    var str = Encoding.ASCII.GetString(bencodedString, index, colon - index);
			var length = Int32.Parse(str);

            if (length.ToString() != str)
                throw Error();

			index = colon + 1;
			var tmpIndex = index;
			index += length;

            if (bencodedString.Length >= tmpIndex + length)
			    return new BString(bencodedString.Skip(tmpIndex).Take(length).ToArray());

		    return null;
		}

	    private static Exception Error()
		{
			return new BencodingException("Bencoded string invalid.");
		}
	}

	public interface BElement
	{
	    MemoryStream ToBencodedString(MemoryStream u);
	}

	public class BInteger : BElement, IComparable<BInteger>
	{
		public static implicit operator BInteger(int i)
		{
			return new BInteger(i);
		}

		public long Value { get; private set; }

		public BInteger(long value)
		{
			Value = value;
		}

		public MemoryStream ToBencodedString(MemoryStream u)
		{
			u.WriteByte((byte) 'i');

			byte[] buffer = Encoding.UTF8.GetBytes(Value.ToString());

			u.Write(buffer, 0, buffer.Length);
			u.WriteByte((byte) 'e');

			return u;
		}

		public override int GetHashCode()
		{
			return Value.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			try
			{
				return Value.Equals(((BInteger) obj).Value);
			}
			catch (InvalidCastException)
			{
			    return false;
			}
		}

		public float ToFloat()
		{
			return Value / 1000000.0f;
		}

		public override string ToString()
		{
			return Value.ToString();
		}

		public int CompareTo(BInteger other)
		{
			return Value.CompareTo(other.Value);
		}
	}

	public class BString : BElement, IComparable<BString>
	{
		public static implicit operator BString(string s)
		{
			return new BString(s);
		}

		public static implicit operator BString(byte[] s)
		{
			return new BString(s);
		}

	    public byte[] Value { get; private set; }

		public BString(string value)
		{
			Value = Encoding.UTF8.GetBytes(value);
		}

		public BString(byte[] value)
		{
			Value = value;
		}

		public MemoryStream ToBencodedString(MemoryStream u)
		{
			var buffer = Encoding.UTF8.GetBytes(Value.Length.ToString());

			u.Write(buffer, 0, buffer.Length);
			u.WriteByte((byte) ':');
			u.Write(Value, 0, Value.Length);
			
			return u;
		}

		public override int GetHashCode()
		{
			return Value.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			try
			{
				return Value.Equals(((BString) obj).Value);
			}
			catch { return false; }
		}

		public override string ToString()
		{
			return Encoding.UTF8.GetString(Value);
		}

		public int CompareTo(BString other)
		{
			for (var i = 0; i < Value.Length; i++)
			{
				if (i >= other.Value.Length)
					return 1;

				var cmp = Value[i].CompareTo(other.Value[i]);

				if (cmp != 0)
					return cmp;
			}

			if (Value.Length < other.Value.Length)
				return -1;

			return 0;
		}
	}

	public class BList : List<BElement>, BElement
	{
		public MemoryStream ToBencodedString(MemoryStream u)
		{
			u.WriteByte((byte) 'l');

			foreach (var element in ToArray())
			{
				element.ToBencodedString(u);
			}

			u.WriteByte((byte) 'e');

			return u;
		}

		public void Add(string[] values)
		{
			foreach (var value in values)
			{
				Add(new BString(value));
			}
		}

        public void Add(BElement[] values)
        {
            foreach (var value in values)
            {
                Add(value);
            }
        }

        public void Add(long[] values)
        {
            foreach (var value in values)
            {
                Add(new BInteger(value));
            }
        }
	}

	public class BDictionary : SortedDictionary<BString, BElement>, BElement
	{
		public MemoryStream ToBencodedString(MemoryStream u)
		{
			u.WriteByte((byte) 'd');

			for (var i = 0; i < Count; i++)
			{
				Keys.ElementAt(i).ToBencodedString(u);
				Values.ElementAt(i).ToBencodedString(u);
			}

			u.WriteByte((byte) 'e');

			return u;
		}

		public void Add(string key, BElement value)
		{
			Add(new BString(key), value);
		}

		public BElement this[string key]
		{
			get
			{
                try
                {
                    return this[new BString(key)];
                }
                catch (KeyNotFoundException e)
                {
                    throw new BencodeKeyNotFoundException(key, e);
                }
			}
		    set
			{
				this[new BString(key)] = value;
			}
		}
	}

    [Serializable]
	public class BencodingException : FormatException
	{
		public BencodingException(string message) : base(message) { }
	}

    [Serializable]
    public class BencodeKeyNotFoundException : KeyNotFoundException
    {
        public BencodeKeyNotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}
