using Implvz.Exceptions;
using Implvz.Game.Objects;
using Implvz.Helpers;
using Implvz.Net;
using Implvz.Net.Helpers;
using Implvz.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Implvz
{
	public class MainClass
	{
		private readonly Dictionary<Socket, MemoryStream> _buffer;
		private readonly Game.Implvz                      _implvz;
		private readonly TCPServer                        _server;

	    public static void Main(string[] args)
		{
            Console.ForegroundColor = ConsoleColor.White;

			var startup = "\n\n";
			startup += "##############################################\n";
			startup += "#                                            #\n";
			startup += "# Starting game codename Implvz, have fun... #\n";
			startup += "#                                            #\n";
			startup += "##############################################\n";

			Debug.WriteLine(startup, Debug.DebugLevels.Always, ConsoleColor.Magenta);

			new MainClass(args);
		}

	    MainClass(IList<string> args)
		{
			try
			{
				ushort port;
				uint   maxPlayers, tickRate;
				Map    map;

				switch (args.Count)
				{
					case 0:
						port = Settings.Default.Port;
						maxPlayers = Settings.Default.MaxPlayers;
						map = new Map(Settings.Default.Map);
						tickRate = Settings.Default.TickRate;
						break;
					case 4:
						port = Convert.ToUInt16(args[0]);
						maxPlayers = Convert.ToUInt16(args[1]);
						map = new Map(args[2]);
						tickRate = Convert.ToUInt32(args[3]);
						break;
					default:
						Console.WriteLine("usage:   implvz [port maxplayers mapfilepath tickrate]");
						Console.WriteLine("example: implvz 1337 10 somefile.bmp 200");
						return;
				}

                var localEndPoint = new IPEndPoint(IPAddress.Any, port);
				_buffer = new Dictionary<Socket, MemoryStream>();

				using (_server = new TCPServer(localEndPoint))
                using (_implvz = new Game.Implvz(maxPlayers, map, tickRate))
                {
                    _implvz.DoDisconnect += DoDisconnect;
                    _implvz.DoSend += DoSend;

                    _server.OnConnect += OnConnect;
                    _server.OnDisconnect += OnDisconnect;
                    _server.OnRecieve += OnRecieve;

                    _server.Start();
                    _implvz.Start();

                    while (true)
                    {
                        _server.Iterate();
                        _implvz.Iterate();

                        Thread.Sleep(1); // Reduces the wasted cpu cycles of the program
                    }
                }
			}
			catch (NoSpawnLocationException)
			{
				Debug.WriteLine("No red pixels found on map!", Debug.DebugLevels.Error);
                Debug.WriteLine("Map load error... (NoSpawnLocation)", Debug.DebugLevels.Always);
			}

            Debug.WriteLine("Server has stopped... Press any key to continue...", Debug.DebugLevels.Always);

			Console.ReadKey();
		}

		private void OnConnect(TCPServer sender, Socket client)
		{
			_buffer.Add(client, new MemoryStream());

			_implvz.OnConnect(client);
		}

		private void OnDisconnect(TCPServer sender, Socket client)
		{
			_implvz.OnDisconnect(client);

			_buffer.Remove(client);
		}

		private void OnRecieve(TCPServer sender, Socket client, byte[] data, int offset, int bytes)
		{
			var buffer = _buffer[client];
			buffer.Write(data, offset, bytes);

			try
			{
				while (true)
				{
					var length = (int) buffer.Length;
					var message = buffer.ToArray().Take(length).ToArray();

					var tbi = BencodeDecoder.Decode(message);
                    if (tbi == null)
                    {
                        _buffer[client] = buffer;
                        return;
                    }

					var tmp = new MemoryStream(length - tbi.Item2);
					buffer.Position = tbi.Item2;
					buffer.CopyTo(tmp);
					buffer = tmp;

					Debug.WriteLine(message.Take(tbi.Item2).ToArray(), Debug.DebugLevels.Protocol, ConsoleColor.Green, ">>");

					if (tbi.Item1.GetType() != typeof (BDictionary))
					{
						DoDisconnect(client);
						return;
					}

					var bd = (BDictionary) tbi.Item1;

					// pings, just ignore the packet ("de")
					if (bd.Count == 0)
						continue;

					// verify exitence of message id
					if (bd[(BString) ""].ToString().Length == 0)
					{
						DoDisconnect(client);
						return;
					}

					_implvz.OnMessage(client, bd);
				}
			}
			catch (KeyNotFoundException)
			{
				Debug.WriteLine(
					"KeyNotFoundException in OnRecieve(TCPServer sender, Socket client, byte[] data, int offset, int bytes)",
					Debug.DebugLevels.Error);
				DoDisconnect(client);
            }
			catch (InvalidClientMessage)
			{
				Debug.WriteLine(
					"InvalidClientMessage in OnRecieve(TCPServer sender, Socket client, byte[] data, int offset, int bytes)",
					Debug.DebugLevels.Error);
				DoDisconnect(client);
            }
			catch (BencodingException)
			{
				Debug.WriteLine(
					"BencodingException in OnRecieve(TCPServer sender, Socket client, byte[] data, int offset, int bytes)",
					Debug.DebugLevels.Error);
				DoDisconnect(client);
            }
		}

		private void DoDisconnect(Socket client)
		{
			_server.Disconnect(client);
		}

		private void DoSend(Socket client, byte[] message)
		{
			_server.Send(client, message, 0, message.Length);
            Debug.WriteLine(message, Debug.DebugLevels.Protocol, ConsoleColor.Cyan, "<<");
		}
	}
}