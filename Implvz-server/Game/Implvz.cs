using Implvz.Exceptions;
using Implvz.Game.Objects;
using Implvz.Helpers;
using Implvz.Net;
using Implvz.Net.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;

namespace Implvz.Game
{
	public sealed class Implvz : IDisposable
	{
		private Map                                    _map;
		private readonly string[]                      _mapList = Map.EnumerateMapsFolder();
		private readonly UInt32                        _maxPlayers;
		private readonly Dictionary<Socket, Player>    _playerSockets;
		private readonly Dictionary<String, Player>    _playerNames;

        private readonly EntityManager                 _entityManager;

		private readonly UInt32                        _tickRate;
		private readonly System.Diagnostics.Stopwatch  _stopwatch = new System.Diagnostics.Stopwatch();
		private          long                          _lastStopwatchTime;

		public delegate  void SendHandler(Socket socket, byte[] message);
		public delegate  void DisconnectHandler(Socket socket);

		public           event SendHandler             DoSend;
		public           event DisconnectHandler       DoDisconnect;

		public Implvz(UInt32 maxplayers, Map map, UInt32 tickRate)
		{
			_maxPlayers    = maxplayers;
			_map           = map;
			_playerSockets = new Dictionary<Socket, Player>();
			_playerNames   = new Dictionary<String, Player>();
            _entityManager = new EntityManager(map);
			_tickRate      = tickRate;

            _entityManager.DoRemoveEntity += OnRemoveEntity;
            _entityManager.DoMoveEntity += DoMoveEntity;
		}

        void DoMoveEntity(Entity e)
        {
            DoBroadcastToAll(Protocol.MkMove(e));
        }
        
		public void Start()
		{
			Debug.WriteLine(String.Format("Server started at {0}", DateTime.UtcNow), Debug.DebugLevels.Always);

			_lastStopwatchTime = 0;
			_stopwatch.Start();
		}

        private void Stop()
        {
            Debug.WriteLine(String.Format("Server stopped at {0}", DateTime.UtcNow), Debug.DebugLevels.Always);

            _stopwatch.Stop();
            _map.Dispose();
        }

		public void OnConnect(Socket socket)
		{
			Debug.WriteLine("OnConnect(Socket socket)");

			Debug.WriteLine(socket.RemoteEndPoint + " connected...", Debug.DebugLevels.Info);
		}

        private void OnChangeHealth(Entity e)
        {
            if ( ! (e is Player)) // Only send this for players
                return;

            Debug.WriteLine("OnChangeHealth(Entity e)");

            DoBroadcastToAll(Protocol.MkHealth(new[] { e as Player }));
        }

        private void OnRemoveEntity(Entity e)
        {
            Debug.WriteLine("OnRemoveEntity(Entity e)");

            DoBroadcastToAll(Protocol.MkRemoveEntity(e));
        }

		public void OnDisconnect(Socket socket)
		{
			Debug.WriteLine("OnDisconnect(Socket socket)");

			try
			{
				DoBroadcastToOthers(socket, Protocol.MkRemoveEntity(_playerSockets[socket]));
				RemovePlayer(socket, _playerSockets[socket].Name);
			}
			catch (KeyNotFoundException) { }
		}

		public void OnMessage(Socket socket, BDictionary message)
		{
			Debug.WriteLine("OnMessage(Socket socket, BDictionary message)");

			var messageId = ((BInteger) message[""]).Value;

		    try
            {
                Player player;

                switch ((Protocol.ClientMessage) messageId)
                {
                    case Protocol.ClientMessage.ServerList:
                        DoSend(socket, Protocol.MkServerList(_playerSockets.Count, _maxPlayers));
                        break;
                    case Protocol.ClientMessage.Login:
                        var name = message["name"].ToString();

                        var loginDebug = "Login attempt by: " + socket.RemoteEndPoint + " - ";

                        if (_playerSockets.ContainsKey(socket))
                        {
                            loginDebug += "Failed - Already logged in";
                            DoDisconnect(socket);
                        }
                        else if (_playerNames.ContainsKey(name))
                        {
                            loginDebug += "Failed - Name already in use";
                            DoSend(socket, Protocol.MkKick("Name already in use"));
                            DoDisconnect(socket);
                        }
                        else if (_playerNames.Count >= _maxPlayers)
                        {
                            loginDebug += "Failed - Server is full";
                            DoSend(socket, Protocol.MkKick("Server is full"));
                            DoDisconnect(socket);
                        }
                        else
                        {
                            var thisPlayer = new Player(name, FindSpawnLocation(), 0);

                            loginDebug += "Succeeded - Name: " + thisPlayer.Name + " Location: " + thisPlayer.Position + " Orientation: " + thisPlayer.Orientation;

                            AddPlayer(socket, name, thisPlayer);
                            _entityManager.CreateEntity(thisPlayer, OnChangeHealth);

                            DoSend(socket, Protocol.MkMap(_map.MapDataPng));
                            DoSend(socket, Protocol.MkClientConnected(_playerNames.Select(a => a.Value.ID).ToArray(), _playerNames.Select(a => a.Key).ToArray()));
                            DoSend(socket, _playerNames.Select(a => Protocol.MkMove(a.Value)).Aggregate((a, b) => a.Concat(b).ToArray()));

                            var playerNames   = _playerSockets.Values.Select(pn => pn.Name).ToArray();
                            var playerScores  = _playerSockets.Values.Select(ps => ps.Score).ToArray();

                            DoSend(socket, Protocol.MkScore(playerNames, playerScores));

                            DoBroadcastToOthers(socket, Protocol.MkClientConnected(new[] { thisPlayer.ID }, new[] { name }));
                            DoBroadcastToOthers(socket, Protocol.MkMove(thisPlayer));
                        }
                        
                        Debug.WriteLine(loginDebug, Debug.DebugLevels.Always);
                        break;
                    case Protocol.ClientMessage.Move:
                        var rot = ((BInteger)message["rot"]).ToFloat();

                        var newPoint = new PointF(((BInteger)message["x"]).ToFloat(), ((BInteger)message["y"]).ToFloat());

                        try
                        {
                            player = _playerSockets[socket];

                            if (_map.IsLegalMove(newPoint, player))
                            {
                                player.NewPosition(newPoint, rot);
                            }
                            else
                            {
                                DoSend(socket, Protocol.MkMove(_playerSockets[socket]));
                            }
                        }
                        catch (KeyNotFoundException)
                        {
                            DoDisconnect(socket);
                        }
                        break;
                    case Protocol.ClientMessage.ChatMessage:
		                var msg = message["message"].ToString();
						
						if (msg.Substring(0, 1) == "/")
                        {
                            // TODO access control!
                            var everything = message["message"].ToString();
                            var command = everything.Substring(1, everything.IndexOf(' '));
                            var arguments = everything.Substring(1 + command.Length, everything.Length);

                            switch (command)
                            {
                                case "die":
									// todo kill player!
                                    break;
								case "kill":
									// todo kill target player
		                            break;
                                case "nextmap":
                                    var nm = String.Empty;

                                    for (var i = 0; i < _mapList.Count(); i++)
                                        if (_map.MapFilePath == _mapList[i])
                                            nm = _mapList[(i + 1) % _mapList.Count()];
									
									// re-initialize the map object
									_map = new Map(nm);
									var theNewMap = Protocol.MkMap(_map.MapDataPng);
		                            
									var mapAndPositions = new MemoryStream();
                                    mapAndPositions.Write(theNewMap, 0, theNewMap.Length);

                                    // send startup positions to all players
                                    foreach (var onePlayer in _playerSockets)
                                    {
                                        onePlayer.Value.NewPosition(_map.GetSpawnLocation(), 0);
                                        onePlayer.Value.ApplyNewPosition();
                                        var move = Protocol.MkMove(onePlayer.Value);
                                        mapAndPositions.Write(move, 0, move.Length);
                                    }

                                    // send map to all players!
                                    // send positions to all players!
                                    DoBroadcastToAll(mapAndPositions.ToArray());
                                    break;
                                default:
                                    DoSend(socket, Protocol.MkChatMessage(_playerSockets[socket], "Unknown Command: " + command));
                                    break;
                            }
                        }

                        // TODO rate limiting...
                        DoBroadcastToAll(Protocol.MkChatMessage(_playerSockets[socket], message["message"]));
                        break;
					case Protocol.ClientMessage.Weapon:
						// TODO cycle to next or previous weapon or specified weapon (if equipped)
		                break;
					case Protocol.ClientMessage.Fire:
						// TODO fire currently selected weapon
                        player    = _playerSockets[socket];
                        var playerPos = player.Position;
                        var playerOri = player.Orientation;

                        var position = new PointF(playerPos.X + (float) Math.Cos(playerOri) * (player.Radius + Projectile.Radius),
                                                  playerPos.Y + (float) Math.Sin(playerOri) * (player.Radius + Projectile.Radius));
                        
                        var projectile = new Projectile(position, playerOri, 12.5f, _playerSockets[socket].Weapon);

                        if (_map.IsLegalMove(position, projectile))
                        {
                            _entityManager.CreateEntity(projectile, OnChangeHealth);

                            DoBroadcastToAll(Protocol.MkMove(projectile));
                        }
                        // TODO damage target player if hit
		                break;
                    default:
                        throw new InvalidClientMessage("Unknown message id: ( " + messageId + " )");
                }
            }
            catch (BencodeKeyNotFoundException e)
            {
                throw new InvalidClientMessage("Missing key ( " + e.Message + " ) from dictionary");
            }
		}

        public void Iterate()
        {
            var time  = _stopwatch.ElapsedMilliseconds;
            var dTime = time - _lastStopwatchTime;

            while (dTime >= _tickRate)
            {
				//Debug.WriteLine("Iterated at " + DateTime.UtcNow, Debug.DebugLevels.Debug, ConsoleColor.DarkGray);

                foreach (var player in _playerSockets)
                {
                    if (float.IsNaN(player.Value.PendingOrientation))
                        continue;

                    if (player.Value.PendingPosition == player.Value.Position && player.Value.PendingOrientation == player.Value.Orientation)
                        continue;

                    player.Value.ApplyNewPosition();

                    DoBroadcastToOthers(player.Key, Protocol.MkMove(player.Value));
                    //player.Value.NewPosition(new PointF(float.NaN, float.NaN), float.NaN);
                }

                _entityManager.UpdateEntities();

                dTime -= _tickRate;
            }

            _lastStopwatchTime = time - dTime;
        }
		
		private void DoBroadcastToAll(byte[] message)
		{
			foreach (var playerSocket in _playerSockets)
				DoSend(playerSocket.Key, message);
		}

		private void DoBroadcastToOthers(Socket notTo, byte[] message)
		{
			foreach (var playerSocket in _playerSockets)
				if (playerSocket.Key != notTo)
					DoSend(playerSocket.Key, message);
		}

		private void AddPlayer(Socket socket, String name, Player player)
		{
			_playerSockets.Add(socket, player);
			_playerNames.Add(name, player);
		}

		private void RemovePlayer(Socket socket, String name)
        {
            _entityManager.RemoveEntity(_playerSockets[socket].ID);
			_playerSockets.Remove(socket);
			_playerNames.Remove(name);
		}

		private Tuple<PointF, float> FindSpawnLocation()
		{
			return new Tuple<PointF, float>(_map.GetSpawnLocation(), (float)(Math.PI / 2));
		}

	    public void Dispose()
	    {
	        Stop();
	    }
	}
}
