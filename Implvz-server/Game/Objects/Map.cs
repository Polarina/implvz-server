using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Implvz.Exceptions;

namespace Implvz.Game.Objects
{
	public sealed class Map : IDisposable
	{
		public           string   MapFilePath { get; private set; }
		public           byte[]   MapDataPng  { get; private set; }

		private readonly Bitmap   _mapData;
		private          PointF[] _spawnLocations;
		private readonly Random   _random = new Random();

		public Map (string mapFilePath)
		{
			MapFilePath = mapFilePath;

			using (var ms = new MemoryStream(File.ReadAllBytes(@"maps/" + mapFilePath)))
			{
				_mapData = new Bitmap(Image.FromStream(ms));
			}

			EnumerateSpawnLocations();


			PNGifyMap();
		}

		public bool IsLegalMove(PointF p, Entity e)
		{
            var sx = (int) Math.Floor(p.X   - e.Radius);
            var sy = (int) Math.Floor(p.Y   - e.Radius);
            var ex = (int) Math.Ceiling(p.X + e.Radius);
            var ey = (int) Math.Ceiling(p.Y + e.Radius);

			var mapWidth  = _mapData.Width;
			var mapHeight = _mapData.Height;

            if (sx < 0 || sx >= mapWidth  ||
                sy < 0 || sy >= mapHeight ||
                ex < 0 || ex >= mapWidth  ||
                ey < 0 || ey >= mapHeight)
                return false;

            for (var i = sx; i < ex; i++)
            {
                for (var j = sy; j < ey; j++)
                {
                    if (_mapData.GetPixel(i, mapHeight - j - 1) == Color.FromArgb(0, 0, 0))
                    {
                        var x = i + 0.5f;
                        var y = j + 0.5f;

                        var distance = new PointF(Math.Abs(p.X - x), Math.Abs(p.Y - y));

                        if (distance.X > 0.5f + e.Radius) continue;
                        if (distance.Y > 0.5f + e.Radius) continue;

                        if (distance.X <= 0.5f) return false;
                        if (distance.Y <= 0.5f) return false;

                        if (Math.Pow(distance.X - 0.5f, 2) + Math.Pow(distance.Y - 0.5f, 2) <= Math.Pow(e.Radius, 2))
                            return false;
                    }
                }
            }

			return true;
		}

		public PointF GetSpawnLocation()
		{
			return _spawnLocations[_random.Next(0, _spawnLocations.Length)];
		}

		private void EnumerateSpawnLocations()
		{
			var spawnLocations = new List<PointF>();

			for (var i = 0; i < _mapData.Width; i++)
			{
				for (var j = 0; j < _mapData.Height; j++)
				{
					if (_mapData.GetPixel(i, j) == Color.FromArgb(255, 0, 0))
					{
						spawnLocations.Add(new PointF(i + 0.5f, _mapData.Height - j - 0.5f));
						_mapData.SetPixel(i, j, Color.White);
					}
				}
			}

			if (spawnLocations.Count == 0)
				throw new NoSpawnLocationException();

			_spawnLocations = spawnLocations.ToArray();
		}

		private void PNGifyMap()
		{
			using (var ms = new MemoryStream())
			{
				_mapData.Save(ms, ImageFormat.Png);

                MapDataPng = ms.ToArray();
			}
		}

		public static string[] EnumerateMapsFolder()
		{
			return Directory.GetFiles(@".\maps");
		}

        public void Dispose()
        {
            _mapData.Dispose();
        }
	}
}
