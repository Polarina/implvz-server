﻿using Implvz.Helpers;
using System;
using System.Drawing;

namespace Implvz.Game.Objects
{
    public abstract class Entity
    {
        private static long   LastID = 0;

        public          long   ID                 { get; private   set; }
        public          PointF Position           { get; protected set; }
        public          float  Orientation        { get; protected set; }
                        
        public          float  Radius             { get; protected set; }
                        
        public          PointF PendingPosition    { get; private set; }
        public          float  PendingOrientation { get; private set; }
        public          float  PendingHealth      { get; private set; }
        public          float  Velocity           { get; protected set; }
        public          float  Health             { get; protected set; }

        public delegate void  HealthChanged(Entity e);
        public          event HealthChanged DoChangeHealth;

        protected Entity(PointF position, float orientation, float velocity)
        {
            ID                 = ++LastID;

            Position           = position;
            Orientation        = orientation;

            PendingPosition    = position;
            PendingOrientation = orientation;
            PendingHealth      = Health;

            Velocity           = velocity;

            Debug.WriteLine(String.Format("Entity: Created new entity with id: {0}, Position: {1}, Orientation: {2}",
                ID,
                Position,
                Orientation), Debug.DebugLevels.Debug, ConsoleColor.Red);
        }

        public void ApplyNewHealth()
        {
            Health = PendingHealth;

            DoChangeHealth(this);
        }

        public void NewHealth(float newHealth)
        {
            PendingHealth = newHealth;
        }

        public void ApplyNewPosition()
        {
            Position    = PendingPosition;
            Orientation = PendingOrientation;
        }

        public void NewPosition(PointF position, float orientation)
        {
            PendingPosition    = position;
            PendingOrientation = orientation;
        }

        public abstract bool Collision(Entity e);

        public Tuple<PointF, bool> Tick(Map map)
        {
            var time = Properties.Settings.Default.TickRate / 1000f;
            var x    = Position.X + Velocity * (float) Math.Cos(Orientation) * time;
            var y    = Position.Y + Velocity * (float) Math.Sin(Orientation) * time;

            NewPosition(new PointF(x, y), Orientation);

            if ( ! map.IsLegalMove(PendingPosition, this)) // it should have already been rejected by the message parser.
                return new Tuple<PointF, bool>(new PointF(x, y), false);

            ApplyNewPosition();

            return new Tuple<PointF, bool>(new PointF(x, y), true);
        }
    }
}
