using Implvz.Game.Objects.Weapons;
using System;
using System.Collections.Generic;
using System.Drawing;
using Implvz.Helpers;

namespace Implvz.Game.Objects
{
	public sealed class Player : Entity
	{
		public string       Name                { get; private set; }
		public Weapon       Weapon              { get; private set; }
		public List<Weapon> Weapons             { get; private set; }
        public long         Score               { get; private set; }

		public Weapon       PendingWeapon       { get; private set; }

		public Player(string name, Tuple<PointF, float> posAndRot, float velocity)
			: this(name, posAndRot.Item1, posAndRot.Item2, velocity) { }

        public Player(string name, PointF position, float orientation, float velocity)
            : base(position, orientation, velocity)
        {
            Radius        = 0.375f;

			Name          = name;
            Health        = 1f;
            Score         = 0;

			var pistol    = new Pistol();
			Weapon        = pistol;
			Weapons       = new List<Weapon>() { pistol };

			PendingWeapon = null;
		}

		public void ApplyChangeWeapon()
		{
			Weapon = PendingWeapon;
		}

		public void ChangeWeapon(Weapon weapon)
		{
			PendingWeapon = weapon;
		}

		public void AddNewWeapon(Weapon weapon)
		{
			Weapons.Add(weapon);
		}

        public void Respawn(PointF newSpawnLocation, float newOrientation)
        {
            NewPosition(newSpawnLocation, newOrientation);
            ApplyNewPosition();

            NewHealth(1f);
            ApplyNewHealth();
        }

        public override bool Collision(Entity e)
        {
            if (e is Projectile)
            {
                NewHealth(Health - .2f);

                Debug.WriteLine(String.Format("Health modified on: {0} New value: {1}", Name, PendingHealth), Debug.DebugLevels.Info, ConsoleColor.Red);

                ApplyNewHealth();

                const float EPSILON = .0000001f;

                if (Health <= EPSILON)
                    return true;
            }

            return false;
        }
	}
}
