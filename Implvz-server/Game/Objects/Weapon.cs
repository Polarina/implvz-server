﻿using System;

namespace Implvz.Game.Objects
{
	public abstract class Weapon
	{
		public    string Name               { get; protected set; }
		public    float  Damage             { get; protected set; }
		public    float  Velocity           { get; protected set; }
		public    float  Range              { get; protected set; }

		protected uint   MaxAmmoLoaded;
		public    uint   CurrentAmmoLoaded  { get; protected set; }

		protected uint   MaxAmmoCarried;
		public    uint   CurrentAmmoCarried { get; protected set; }

		public   float   RateOfFire         { get; protected set; }
		public   float   ReloadTime         { get; protected set; }

		public void Fire()
		{
			if (CurrentAmmoCarried > 0)
				CurrentAmmoCarried--;
		}

		public bool CanFire()
		{
			return CurrentAmmoCarried > 0;
		}

		public void AddCarryAmmo(uint newCarryAmmo)
		{
			CurrentAmmoCarried = Math.Min(MaxAmmoCarried, CurrentAmmoCarried + newCarryAmmo);
		}

		public void Reload()
		{
			var difference = Math.Min(CurrentAmmoCarried, MaxAmmoLoaded - CurrentAmmoLoaded);
			CurrentAmmoCarried -= difference;
			CurrentAmmoLoaded  += difference;
		}
	}
}
