﻿namespace Implvz.Game.Objects.Weapons
{
	public sealed class Bazooka : Weapon
	{
		public Bazooka()
		{
			Name               = "Bazooka";
			Damage             = 1f;
			Velocity           = 5f;
			Range              = 20f;

			MaxAmmoLoaded      = 1;
			CurrentAmmoLoaded  = 1;

			MaxAmmoCarried     = 3;
			CurrentAmmoCarried = 9;

			RateOfFire         = 2f;
			ReloadTime         = 5f;
		}
	}
}
