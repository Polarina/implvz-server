﻿namespace Implvz.Game.Objects.Weapons
{
	public sealed class Pistol : Weapon
	{
		public Pistol()
		{
			Name               = "Pistol";
			Damage             = 0.1f;
			Velocity           = 3f;
			Range              = 10f;

			MaxAmmoLoaded      = 8;
			CurrentAmmoLoaded  = 8;

			MaxAmmoCarried     = 200;
			CurrentAmmoCarried = 200;

			RateOfFire         = 0.2f;
			ReloadTime         = 2f;
		}
	}
}
