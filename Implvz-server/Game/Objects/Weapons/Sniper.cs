﻿namespace Implvz.Game.Objects.Weapons
{
	public sealed class Sniper : Weapon
	{
		public Sniper()
		{
			Name               = "Sniper";
			Damage             = 1f;
			Velocity           = 1000f;
			Range              = 1000f;

			MaxAmmoLoaded      = 10;
			CurrentAmmoLoaded  = 10;

			MaxAmmoCarried     = 20;
			CurrentAmmoCarried = 20;

			RateOfFire         = 1f;
			ReloadTime         = 4f;
		}
	}
}
