﻿using System.Drawing;

namespace Implvz.Game.Objects
{
    public sealed class Projectile : Entity
    {
        public            Weapon Weapon { get; private set; }
        public new static float  Radius = 0.175f;

        public Projectile(PointF position, float orientation, float velocity, Weapon weapon)
            : base(position, orientation, velocity)
        {
            Weapon = weapon;
            Radius = 0.175f;
            Health = .1f;
        }

        public override bool Collision(Entity e)
        {
            return true;
        }
    }
}
