﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Implvz.Game.Objects.Vehicles
{
    public sealed class Buggy : Vehicle
    {
        public Buggy(PointF location, float orientation, float velocity)
            : base(location, orientation, velocity)
        {
            Name         = "Buggy";
            Mass         = 1000000f;
            Acceleration = 9.8f;
            MaxVelocity  = 10f;
            Health       = 1f;
        }

        public override bool Collision(Entity e)
        {
            throw new NotImplementedException();
        }
    }
}
