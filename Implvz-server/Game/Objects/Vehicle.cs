﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Implvz.Game.Objects
{
    public abstract class Vehicle : Entity
    {
        public string Name         { get; protected set; }
        public float  Mass         { get; protected set; } // Mg
        public float  Acceleration { get; protected set; } // m/s^2
        public float  MaxVelocity  { get; protected set; } // m/s
        
        protected Vehicle(PointF location, float orientation, float velocity)
            : base(location, orientation, velocity)
        {

        }
    }
}
